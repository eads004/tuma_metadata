1
DeVinney/ Blue/Rosen
Treatment #201/Fina l Draft
October 7, 1988
EYES ON THE P RIZE, 1965 - 198?
Program #201 : "Emerging Voices, 1965 - 1966"

APPROACH
The first series of programs for EYES ON THE PRIZE ended with
a triumph. Black and white citizens of America came together to
march the fift y - f our miles of highway 81 connecting Selma, Alabama to the state capital of Montgomer y

the cradle of the Con-

federacy. The issue was voting rights. The cause was justice. The
result was the Voting Rights Act of 1965.

By law, all Un ited States citizens now had the right to vote,
especially black reside'nts of Southern cities and towns where
that right had been denied them for so long. No wonder 25,000
marchers stood in the rain that day, March 25, 1965, and cheered
Dr. Hartin Luther King as television reported the event. But the
television crews went in search of a new story, the crowds went
home and laws do ~· t enforce themselve s.

By 1965, l a ws and Supreme Court decisions were nothing new to
the black commu ni ty. There had been Civil Right Acts in 1957,
1960, and 1964 . With passage of the Voting Rights Act of 1985,

2

one member of the staff for the Southe rn Christ ian Leader ship
Confer ence believe d their work was done . "There is no more civil
rights moveme nt. Presid ent Johnso n signed it out of existen ce
when he signed the voting rights bi 11." He was wrong.

It would take more than legisla tion to reverse years of discrimin ation and racism which had built segreg ation into major
institu tions of Americ a. When white Americ ans sensed social and
econom ic threats in the areas of school , housing and employ ment,
resista nce was enormo us. In respon se, the Moveme nt needed to
develop new goals and create new strateg ies.

In the south, nonvio lent direct action had been the strateg y
of the civil rights moveme nt for the past eleven years. The
nation was familia r with images of large groups of peacef ul
blacks who protest ed for their rights by praying , marchi ng, and
sitting in. But, during the next fifteen months , some member s of
the Studen t Nonvio lent Coordi nating Commit tee (SNCC) found new
ways to challen ge the contra diction s of Americ an society . They
created indepen dent politic al partie s, shifted from passive resistanc e to self-de fense, and questio ned the role of whites in
the moveme nt. In June of 1968, during a rally in Greenw ood,
Missis sippi, Stokely Carmic hael calls for "Black Power" -- a

3

phrase both exhila rating and ambigu ous, a phrase that set off a
wave of controv ersy within the Moveme nt and through out America
over its hidden meanin gs and hidden threat s.

Program #201 docume nts that shift toward Black Power. It was

a logica l shift, born of the disillu sionme nt of civil rights
worker s who toiled long and hard for modest results ; whose efforts were frustra ted,

indeed thwarte d, by southe rn violenc e and

nation al ambiva lence . We focus primar ily on the efforts of SNCC a
group of young people who pursued the Americ an Dream. They had
campaig ned in many southe rn commu nities armed with legal rights
and moral persua sion. But that's not enough . The law fails to
protec t them and Americ an morali ty is a sometim e thing.

Since the early 1960's , severa l SNCC member s adnired Malcolm
X's philoso phy. He, in turn, felt "the studen ts" (as he called
them) were the most radica l of the civil rights o r ganiza tions. We
show how SNCC begins to change in 1964, a conseq uence of Missis sippi's Freedom Summer . The change escala tes the followi ng fall
when severa l of its member s tour Africa . An intima te associa tion
with Malcolm X shortly before his assass ination enriche s SNCC
philoso phy . The emerge nce of a new leader, Stokely Carmic hael,
gives the Studen t Nonvio lent Coordi nating Commi ttee a fresh perspectiv e, one which finds expres sion in the call for Black Power
during James Mered ith's March Agains t Fear.

4

DRAMATIC STRUCTURE
SEQUENCE ONE: Series Tease (3-4 minutes)

We open with a series of images from the final show in the
original series: SNCC students in Selma chanting "Freedom, freedom, freedom ... ;" crossfade to music -- "Marching on to Freedom
Land" -- and we are in the final march from Selma to Montgomery.
Narration sets the scene, the triumphant end of a struggle in the
South against the evils of jim crow. We cut to a portion of the
Montgomery speech given by Dr. Martin Luther King as he warns
there are many dark hours ahead.

New images fill the screen to reveal scenes from series II.
Narration drops in occasionally to points out major themes in
this new series of shows. Words and images will show that segregation and racism were not limited to any one section of the
country and the struggle for racial equality was long and complex. Exact scenes for this segment will be selected from all
eights shows. Final determination s will be made in late Spring,
1989 as all shows approach fine cut and series images can be
readily identified.

STANDARD OPEN

.J

5

SEQUENCE TWO: The North and Malcolm X
We begin in Harlem, 1959. Briefly, we set the scene of crowded tenements and the urban ghetto. While the Civil Rights Movement is forming in the South, the plight of America's black
population in the North are ignored except for the work of one
major organizati on -- the Nation of Islam. Based in Chicagom its
leader is Elijah Muhammed and here, in New York City, his principal spokesman is a man known as Malcolm X.

Most blacks are slow to accept Malcolm X and the Nation of
Islam. The goal, to them, has always been integratio n and the
Nation's program is too radical. Goals are centered around themes

J

of Black Nationalis m, separatism , community control, black pride
and self-defen se. These ideas have started to find an
and, at this time, membership in the Nation has grown
than 200,000.

White response is captured in a 1959 documenta ry entitled
"The Hate That Hate Produced." Produced by Hike Wallace with
Louis Lomax, it presents Malcolm X and the Nation of Islam as
preachers of racial hate.

It is not hate that Malcolm X preaches. He points out the
futility of black lives if they wait for the white man to solve
their problems for them. He exhorts black people to take respon-

6

white
sibili ty for thems elves and to protec t thems elves from the
of
man's laws and the white man's promi ses. Throug h a series
that
sound bites, we hear Malcol m articu late many of the conce rns
black Ameri cans feel.

This sectio n will also contai n brief biogra phica l data on
Malcol m X's backg round.

Our prima ry story teller s are:
Earl Grant , a membe r of the Nation of Islam, who descri bes
Harlem in this period , expla ins why he joined the Nation of
Islam, and descri bes his first memory of Malco lm.
Betty Shaba zz, Malcol m X's wife, who recal ls when she first
met Malcol m X and expla ins her membe rship in the Nation .
in
Mike Walla ce, anchor man for a nightl y news progra m on WTNA
New York who presen ted the first major televi sion progra m about
the Nation of Islam and Malcol m X.

ca
SEQUENCE THREE: Expan ding influe nce of Malcol m X on Black Ameri

Malcol m X must be seen as more than a stree t corne r orator
who appea ls only to Ameri ca's downt rodden . His ideas and vision
ecbegin to find an expan ding audien ce among cultu ral and intell
tual leader s of Black Ameri ca. In this sectio n, we will show

7

Malcolm X as they saw him and understo od him. Through these
intervie ws, we come to understa nd his influenc e on the future of
the Black Movemen t.

Principa l storyte llers include:
Peter Bailey, a civil rights worker from the South who moved
to Harlem in 1962, discover ed Malcolm speaking on a street corner, and was forever changed . Bailey explains what Malcolm said
that traditio nal civil rights leaders were not saying.
Alex Haley who knew of Malcolm even before he was invited to
write "The Autobiog raphy of Malcolm X." Part of the cultural
communi ty, he was aware of the influenc e Malcolm was having at
all levels of society.
Stokely Carmich ael and Clevelan d Sellers heard Malcolm X
speak in 1962 when they were students at Howard Univers ity and
greatly respecte d what he said.
SEQUENCE FOUR: Expulsio n and transfor mation

In Novembe r, 1963, Presiden t John F. Kennedy is assassin ated.
Malcolm describe s the violence as "the chickens coming home to
roost." The comment draws censure and eventua l expulsio n from the
Nation of Islam. The expulsio n actually has a liberati ng effect
on Malcolm . He no longer speaks for Elijah Muhammed, he becomes

8

his own man and speaks for himsel f. He has fifteen months to live
and during that time, he makes a series of bold steps which
expand his role as a philoso pher and leader for Black Americ ans.

By summer , 1964, Malcolm X ferns two new organi zations : the
Organi zation of Afro-A merican Unity (OAAU) to work for black
unity and freedom in cooper ation with other civil rights groups ;
and Muslim Mosque , Inc. (MMI) which will speak to the religio us
side of his belief s. He makes two trips to Africa and is soon
viewed by the African nation s as a statesm an, a repres entativ e of
black Americ a. A pilgrim age to Mecca broaden s his view of brotherhood which now include s whites .

On one of his trips to Africa , Malcolm meets severa l member s
of the Studen t Nonvio lent Coordi nating Commi ttee. SNCC member s
are recove ring from an exhaus ting voter registr ation campai gn in
Missis sippi, Missis sippi Freedom Summer , which saw the violen t
death of severa l co-wor kers. They were also bitterl y disapp ointed
by the failure to seat the Missis sippi Freedom Democ ratic Party
at the Democ ratic Nation al Conven tion in Atlant ic City.

Follow ing this meetin g, Malcolm X and the Studen t Nonvio lent
Coordi nating Commit tee develo p a relatio nship that puts them in
regula r contac t: Decemb er, 1964, Malcolm X speaks at a MFDP
Harlem rally and, in return , Fannie Lou Hamer and the SNCC Free-

8

dom Singer s speak to his OAAU rally; Decemb er 31, 1864, in
Harlem , he speaks to 37 teenag e SNCC worker s from McComb, Missis sippi. In Februa ry 1865, he addres ses blacks at a voting rights
campaig n in Selma, Alabam a.

Major storyt ellers:
Peter Bailey , who was involve d in plans for the foundin g of
OAAU and can talk to Malcol m's object ives for that organi zation.
Earl Grant, head of MMI who can describ e the change in Malcolm's Muslim though t during this period .
Stokely Carmic hael and/or Clevela nd Sellers about SNCC's work
with Malcolm X.
Mike Wallace who intervi ewed Malcolm shortly after his return
from Africa and saw a differe nt Malcolm than the one he reporte d
on in 1959.
Alex Haley who agrees to write Malcolm X's autobio graphy .
SEQUENCE FIVE: Malcol m's death

Follow ing his return from Selma, Malcolm experie nces tremen dous pressu re in the remain ing two weeks of his life. A trip to
France is aborted when the French governm ent refuses to allow him
entran ce, his home is firebom bed, and he is harrass ed by anonymous phone calls in the night.

10
On February 21, 1965, Malcolm Xis assassin ated in New York
City. CORE and SNCC are the only two major civil rights organiza tions which attend the funeral . Intervie wees will also comment on
what they perceive as the legacy of Malcolm X.

Intervie ws:
Earl Grant who served as Malcolm ' s bodygua rd during that last
week of Malcolm 's life and was present at the assassin ation.
Alex Haley who, through regular phone convers ations with Malcolm, served as a sort of emotion al sounding board during this
trying time .
Betty Shabazz who saw Malcolm 's tensions increase as he
struggle d against so many pressure s .
Peter Bailey who spoke to Malcolm about the future of OAAU
just fifteen minutes before the assassin ation took place.
Clevelan d Sellers who represen ted SNCC at Malcolm 's funeral.
SEQUENCE SIX: Summer, 1965

By summer, black culture takes to the streets of Harlem in
the form of musical performa nces, dancers and poetry readings .
Signs in book store windows promote books about Malcolm X, as
well as a book entitled : "The Goddam White Man." Congress man Adam
Clayton Powell believes that a new anti-pov erty program, part of
Presiden t Johnson 's War on Poverty, will help to prevent a
reprisal of last summer's riots.

11

On August 8, 1965, President Johnson signs the Voting Rights
Act into law. Only a few days after the signing of the VRA, the
Watts section of Los Angeles erupts. It is a rebellion unlike any
the country has witnessed in the past few years. President Johnson warns that continued violence of this sort could take away
many of the gains of recent civil rights legislatio n. Martin
Luther King visits Watts and concludes that economic injustice is
the primary cause for the riots. He asks for release of poverty
funds for Watts which have been held up by political problems.

This is principal ly a transition sequence to set the scene
for the Lowndes County story which follows. It will be done
entirely with stock footage.
SEQUENCE SEVEN: The Changing Face of SNCC

SNCC members working in Lowndes County, Alabama, hear about
Watts while sitting in the Haynevill e jail. "It made us feel like
we were part of something very big," says one. In fact, they all
are part of something big -- a project headed up by Stokely
Carmichae l in cooperatio n with local leader John Hulett.

Several months earlier, John Hulett was the first black to
register in Lowndes County. Now, as the leader of the Lowndes
County Christian Movement for Human Rights (LCCMHR), he is
~

preparing to take on the county's white power structure. Although

12
blacks represen t BOX of the Lowndes County' s populati on, no
blacks were register ed to vote until this year (1965). On the
other hand, 118X of the white populati on is register ed .

The work has been agonizin gly slow -- only 50-60 people were
register ed between March and August. Now, with passage of the
Voting Rights Act, the first federal registra rs enter the county
and registra tion increase s. So does white resistan ce. Black tenant farmers complain of loan foreclos ures and eviction s. Many
demonst rators are arrested .

One of the big issues SNCC has to confron t in Lowndes County
is the carrying of weapons. Robert Strickla nd, a residen t, carries a gun and, despite Carmich ael's best argumen ts won't give it
up. "You turn the other cheek and you ' ll get handed half of what
you're sitting on." 11..an.y other residen ts also carry guns and
before long some members of SNCC do the same.

And there is violence . On August 20 , a group of SNCC members
and SNCC supporte rs are released from the Haynevi lle jail where
they have been held since their arrest during a demonst ration ten
days earlier. Among them are ministe rial student Jonathan Daniels
and Father Richard Morrisro e, a Catholic priest . These two white
men were cause for an argumen t within SNCC the night before they

13
ld not be part of
were arre sted . Some mem bers felt whit es shou
argu es in thei r beha lf
thes e dem onst ratio ns. Stok ely Carm icha el
and wins .
the grou p, inclu ding
Now, follo wing thei r rele ase, seve ral in
in sear ch of some thing
Dan iels and Fr. Mor risro e, ente r a stor e
appe ars, wiel ding a
to drin k. A Lown des Coun ty depu ty sher iff
Dan iels dies imme dishot gun whic h he fire s with out prov ocat ion.
The shoo ting has a
atel y. Fr. Mor risro e is seve rely woun ded.
elan d Sell ers says , "I
pow erfu l effe ct on Stok ley Carm icha el. Clev
c thin g Stok ely ever
thin k that was prob ably the most trau mati
expe rien ced. "

real ly want to be
Lown des Coun ty blac ks ques tion whet her they
them seco nd clas s citi part of a poli tica l part y whic h has kept
viol ence , the Demo zens . In addi tion to thes e acts of offi cial
ly host ile to them .
crat ic Part y has been cons isten tly and open
plan ning to run for
When Part y offi cial s lear n that blac ks are
from $50 to $500 . The
poli tica l offi ce, they rais e filin g fees
Alab ama cont ains the
offi cial embl em of the Dem ocra tic part y in
Gove rnor Geor ge Walword s "Wh ite Supr emac y," and part y lead er,
famo us word s "Seg relace , seve ral year s earl ier had utte red the
egat ion fore ver. " And ·
gati on now, segr egat ion tomo rrow and segr
' s wife , Lurl een is
now, unab le to chan ge Alab ama law, Wall ace

)

14
running for Governo r in his place. She is being challeng ed on the
Democra tic ticket by Alabama State Attorney General Richmond
Flowers.

SNCC discove rs an obscure Alabama law which allows residen ts
of a county to form an independ ent politica l party. The Lowndes
County Freedom Organiz ation becomes officia l in March, 1968. The
party chooses a black panthers as its emblem. Accordin g to John
Hulett, "The black panther is an animal that, when it is pressured, it moves back until it is cornered . Then it cones out
fighting for life or death. We felt we had been pushed back long
enough and that it was time for black people to come out and take
over."

May 3, 1966, the Alabama Democra tic Party holds its primary .
On the same day, 900 black Lowndes County residen ts, nany traveling 20-25 miles to attend, come together for the first LCFO
convent ion. They elect a slate of seven black candidat es for
local county offices. Particip ants describe their exciteme nt at
seeing so many black people involved in this burgeoni ng politica l
movemen t.

Five days later, an event occurs which signals a change in
directio n for SNCC. At a SNCC retreat held at Kingston Springs
near Nashvil le, Tenness ee, John Lewis is defeated by Stokely
)

15
Carmichae l in his bid for re-electio n as national chairman.

The Players:
Stokely Carmichae l, SNCC
Bob Mants, the primary leader for SNCC in Lowndes County .
John Hulett, resident of Lowndes County who began the drive
for voter registrati on.
Ruby Sales, a SNCC worker who was arrested with Jonathan
Daniels and was present when he was shot.
Richmond Flowers, Attorney General for for the State of Alabama who denounced Governor Wallace for his segregati onist
stand, yet feared the Klan in Lowndes County. In 1966, he runs
for governor against Lurleen Wallace .
Lowndes County residents who can recall their feelings during
this campaign.
SEQUENCE EIGHT: Black Power

June 6, 1966. James Meredith is shot in Mississip pi while
leading a "March Against Fear." All the major civil rights organizations rush to his cause with promises to continue the march
for him.

Conflicts quickly develop. SNCC's new chairman, Stokely
Carmichae l, and Cleveland Sellers take a militant stand on sever-

/

al specific points. They argue that white participat ion is unnec-

16
essary in the march. King disagre es with this positio n and during
rallies along the way he welcom es white suppor ters. Noneth eless,
the role of whites is deemph asized and they are less visible than
in the past.

Carmic hael and Seller s also insist that the Deacon s for Defense provide armed protec tion for the marche rs. Dr. King tolerates the Deacon s' presen ce, but is fearfu l that their particp ation may provok e even greate r white violenc e. "I'm sick and tired
of violenc e. I'm tired of the war in Vietnam . I'm tired of war
and confli cts in the world. I'm tired of shootin g. I'm tired of
hatred . I'm tired of selfish ness. I'm tired of evil. I'm not
going to use violenc e, no matter who says it."

Althoug h King and SCLC and Floyd McKiss ick of CORE try to
work with the SNCC demand s, Roy Wilkin s (NAACP) and the Whitne y
Young, Jr. (Urban League ) abandon the march immed iately. With
fewer nation al civil rights leaders partici pating , the march
takes on a quality unlike previou s marche s. McKiss ick recalls
"This was a march of the common people . We had people who had
never taken part in demon stration s before . But they'd see us
walkin g by and they'd join in."

Along the way, voter registr ation become s an ongoing part of
)

the daily program in each city and town the march passes through .

17
One of the most emotional noments in the march occurs in
Batesville when El Fondren, a 106-year-old retired farmer, registers to vote. He is promptly lifted on the shoulders of marchers
in a triumphant gesture.

June 16. Stokely Carmichael excites an audience in Greenwood,
Mississippi with shouts of "Black Power." The phrase catches on
among the marchers. King is concerned that the phrase may drive
an even deeper wedge between the civil rights movement and white
supporters, already fearful since Watts. Ironically, his fears
appear justified by an event taking place in California: in the
gubernatorial primaries, Ronald Reagan wins overwhelmingly on the
Republican ticket -- a sign, political observers believe, of a
conservative trend sweeping America.

June 21. The march stops in Philadelphia, Mississippi to
observe the second anniversary of the disappearance of the three
civil rights workers who died during Freedom Summer (Chaney,
Goodman and Schwerner). The marchers are attacked by a crowd of
whites. Local police do nothing to stop it until blacks begin
fighting back. Two days later, June 23, state troopers use tear
gas when marchers try to set up tents on school grounds in Canton, Mississippi. King is heard to say, "The government has got
to give me some victories if

)

r·m

going to keep people nonvio-

lent."

/

18
The March Against Fear finally ends in Jackson on June 26.
4,000 blacks in Mississ ippi have register ed to vote, the nost
success ful voter registra tion effort the state has yet seen.
While SNCC support ers affix "Black Power" bumper stickers to
Jackson police cars,

leaders address the 12,000 to 15,000 singing

marchers who crowd around the State CapitoJ. . Dr . King wearily
admits that his "dream has turned into a nightma re." Stokely
Carmich ael is cheered when he declares that blacks must "build a
power base so strong that we will bring whites to their knees
every time they mess with us." As SCLC marchers take up a chant
of "freedom , freedom ," SNCC support ers shout back "Black Power."

The Players:
Floyd McKissi ck, director for CORE, who was sympath etic to
and encourag ed SNCC's radicali sm on the march.
Stokely Carmich ael in the first major campaign of his tenure
as SNCC's nationa l chairman .
Clevelan d Sellers, a march organize r for SNCC.
Andrew Young, executiv e director for the SCLC who handled
many of the march's logistic s in behalf of SCLC.
Clare Maier, a white woman who joins the March after the call
for Black Power and sees King losing ground to the more militan t
factions of the movemen t.

J

