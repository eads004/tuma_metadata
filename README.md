# Sample Code for Tuma's Metadata Project #

## Process ##

1.  Saved spreadsheet as a csv.

2.  Installed The Fuzz (a fork of fuzzywuzzy)

> pip install thefuzz\[speedup\]

3.  Run notebook match_named_entities_and_lcsh_entries.ipynb

4.  Review results in matching_results.csv
    
